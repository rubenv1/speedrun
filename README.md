
## How to run

```
yarn install
yarn start
```

## Architecture
- Use of React.
- Data stored in Redux to enable reusability. This would enable further optimizations like avoiding re-fetching data when browsing back to a previous route.
- Use of Redux thunks, that enable more granular actions
- Use of selectors to make data reusability possible without introducing complexity
- Use of ImmutableJS to avoid object mutation
- Routing done through Redux bindings for React Router
- Use of Moment for time formatting.


## Next tasks
- CSS
- Unit testing
- More detailed component structure
- Pagination
- More complex logic to avoid re-fetching existing data in Redux
- I18n
