import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { Switch, Route } from "react-router-dom";
import { ConnectedRouter } from 'connected-react-router';
import store from './reduxStore';
import history from './browserHistory';

import Games from './containers/Games';
import Run from './containers/Run';

class App extends Component {
  render() {
    return (
      <div>
        <header className="App-header">
          <h1 className="App-title">SpeedRun</h1>
        </header>
        <Provider store={store}>
          <ConnectedRouter history={history}>
            <Switch>
              <Route exact path="/" component={Games} />
              <Route path="/:gameId/first" component={Run} />
            </Switch>
          </ConnectedRouter>
        </Provider>
      </div>
    );
  }
}


export default App;
