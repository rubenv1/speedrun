import { createStore, applyMiddleware, compose } from 'redux';
import { connectRouter, routerMiddleware } from 'connected-react-router';
import thunk from 'redux-thunk';
import rootReducer from './reducers';
import history from './browserHistory';

export default createStore(
  connectRouter(history)(rootReducer),
  compose(
    applyMiddleware(
      thunk,
      routerMiddleware(history)
    )
  )
);
