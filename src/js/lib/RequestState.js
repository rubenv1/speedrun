import { Record } from 'immutable';

import keyMirror from 'fbjs/lib/keyMirror';

const States = keyMirror({
  NOT_REQUESTED: null,
  REQUESTED: null,
  SUCCEEDED: null,
  FAILED: null,
});

export default class RequestState extends Record({
  state: States.NOT_REQUESTED,
}) {
  isNotRequested() {
    return this.state === States.NOT_REQUESTED;
  }
  isPending() {
    return this.state === States.REQUESTED; 
  }
  isSuccessFul() {
    return this.state === States.SUCCEEDED;
  }
  isFailed() {
    return this.state === States.FAILED;
  }
  shouldBeRequested() {
    return this.isNotRequested() || this.isFailed();
  }
  shouldShowAsLoading() {
    return this.isNotRequested() || this.isPending();
  }
};

export const NOT_REQUESTED = new RequestState({state: States.NOT_REQUESTED});
export const REQUESTED = new RequestState({state: States.REQUESTED});
export const SUCCEEDED = new RequestState({state: States.SUCCEEDED});
export const FAILED = new RequestState({state: States.FAILED});
