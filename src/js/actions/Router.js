import { push } from 'connected-react-router'

export const goTo = (path) => (dispatch) => {
  dispatch(push(path));
};
