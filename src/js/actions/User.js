import * as UsersClient from '../api/UserClient';
import actionWithPayload from '../utils/actionWithPayload';
import UsersActionTypes from '../actionTypes/Users';

const parseUser = (user) => {
  return {
    id: user.data.id,
    name: user.data.names.international,
  };
}

export const getUser = (userId) => (dispatch) => {
  dispatch(actionWithPayload(UsersActionTypes.REQUEST_USER));
  UsersClient.getUser(userId)
    .then((response) => {
      dispatch(
        actionWithPayload(
          UsersActionTypes.SET_USER,
          parseUser(response.data)
        )
      );
      dispatch(actionWithPayload(UsersActionTypes.REQUEST_USER_SUCCESS));
    })
    .catch(() => {
      dispatch(actionWithPayload(UsersActionTypes.REQUEST_USER_ERROR));
    });
};
