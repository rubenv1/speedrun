import { Map } from 'immutable';
import * as GamesClient from '../api/GamesClient';
import actionWithPayload from '../utils/actionWithPayload';
import GamesActionTypes from '../actionTypes/Games';

const parseGames = (response) => {
  return response.data
    .reduce((acc, game) => {
      return acc.set(
        game.id, 
        new Map({
          id: game.id,
          name: game.names.international,
          logo: game.assets.logo.uri
        })
      );
    }, new Map());
};

export const getGames = () => (dispatch) => {
  dispatch(actionWithPayload(GamesActionTypes.REQUEST_GAMES));
  GamesClient.getGames()
    .then((response) => {
      dispatch(
        actionWithPayload(GamesActionTypes.SET_GAMES, parseGames(response.data))
      );
      dispatch(actionWithPayload(GamesActionTypes.REQUEST_GAMES_SUCCESS));
    })
    .catch(() => {
      dispatch(actionWithPayload(GamesActionTypes.REQUEST_GAMES_ERROR));
    });
};
