import * as RunsClient from '../api/RunClient';
import actionWithPayload from '../utils/actionWithPayload';
import RunsActionTypes from '../actionTypes/Runs';
import {getUser} from '../actions/User';

const parseRun = (run) => {
  return {
    id: run.id,
    userId: run.players[0].id,
    gameId: run.game,
    timeInMs: run.times.primary_t * 1000,
  }
}

export const getRuns = (gameId) => (dispatch) => {
  dispatch(actionWithPayload(RunsActionTypes.REQUEST_RUNS));
  RunsClient.getRuns(gameId)
    .then((response) => {
      const firstRun = response.data.data[0];
      dispatch(
        getUser(firstRun.players[0].id)
      );
      dispatch(
        actionWithPayload(
          RunsActionTypes.SET_FIRST_RUN, 
          parseRun(firstRun)
        )
      );
      dispatch(actionWithPayload(RunsActionTypes.REQUEST_RUNS_SUCCESS));
    })
    .catch(() => {
      dispatch(actionWithPayload(RunsActionTypes.REQUEST_RUNS_ERROR));
    });
};
