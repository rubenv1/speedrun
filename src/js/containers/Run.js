import React, { Component } from 'react';
import PropTypes from 'prop-types'
import { connect } from 'react-redux';
import moment from 'moment';
import 'moment-duration-format';

import { getGames } from '../actions/Games';
import { getRuns } from '../actions/Runs';
import { selectGame, selectGamesRequestState } from '../selectors/games';
import { selectFirstRun, selectRunsRequestState } from '../selectors/runs';
import { selectUser, selectUserRequestState } from '../selectors/users';

class Games extends Component {
  propTypes: {
    match: PropTypes.object.isRequired,
    getGames: PropTypes.func.isRequired,
    getRuns: PropTypes.func.isRequired,
    game: PropTypes.object.isRequired,
    gamesRequestState: PropTypes.func.isRequired,
    run: PropTypes.object.isRequired,
    runRequestState: PropTypes.object.isRequired,
    user: PropTypes.object.isRequired,
    userRequestState: PropTypes.object.isRequired,
  }

  componentWillMount() {
    const { match } = this.props;
    const gameId = match.params.gameId;

    this.props.getGames();
    this.props.getRuns(gameId);
  }

  renderRun() {
    const { game, run, user } = this.props;

    return (
      <div>
        <div><img src={game.get('logo')} alt="logo"/></div>
        <div>game name: {game.get('name')}</div>
        <div>time: {moment.duration(run.get('timeInMs'), 'ms').format()}</div>
        <div>user: {user.get('name')}</div>
      </div>
    );
  }

  renderLoading() {
    return (
      <div>loading run...</div>
    );
  }

  render() {
    const { gamesRequestState, runRequestState, userRequestState } = this.props;

    let content;
    if (
      gamesRequestState.shouldShowAsLoading() ||
      runRequestState.shouldShowAsLoading() ||
      userRequestState.shouldShowAsLoading()
    ) {
      content = this.renderLoading();
    } else {
      content = this.renderRun();
    }

    return (
      <div className="run">
        {content}
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  const gamesRequestState = selectGamesRequestState(state);
  const runRequestState = selectRunsRequestState(state);
  const userRequestState = selectUserRequestState(state);

  const gameId = ownProps.match.params.gameId;
  const game = selectGame(state, gameId);
  const run = selectFirstRun(state, gameId);
  const user = run ? selectUser(state, run.get('userId')) : null;
  return {
    gamesRequestState,
    runRequestState,
    userRequestState,
    game: game || new Map(),
    run: run || new Map(),
    user: user || new Map(),
  };
};

const actionCreators = {
  getGames,
  getRuns,
};

export default connect(mapStateToProps, actionCreators)(Games);
