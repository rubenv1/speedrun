import React, { Component } from 'react';
import PropTypes from 'prop-types'
import { connect } from 'react-redux';

import { getGames } from '../actions/Games';
import { goTo } from '../actions/Router';
import { selectGames, selectGamesRequestState } from '../selectors/games';

class Games extends Component {
  propTypes: {
    getGames: PropTypes.func.isRequired,
    gamesRequestState: PropTypes.object.isRequired,
    games: PropTypes.array.isRequired,
  }

  componentWillMount() {
    this.props.getGames();
  }

  handleGameClick(gameId) {
    this.props.goTo(`/${gameId}/first`);
  }

  renderGames() {
    const { games } = this.props;

    return games.valueSeq().map(game => {
      return (
        <div onClick={() => this.handleGameClick(game.get('id'))}>
          <img src={game.get('logo')} alt="logo" />
          <span>{game.get('name')}</span>
        </div>
      );
    });
  }

  renderLoading() {
    return (
      <div>loading games...</div>
    );
  }

  render() {
    const { gamesRequestState } = this.props;
    const content = gamesRequestState.shouldShowAsLoading() ?
      this.renderLoading() :
      this.renderGames();

    return (
      <div className="games">
        {content}
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    gamesRequestState: selectGamesRequestState(state),
    games: selectGames(state),
  }
};

const actionCreators = {
  getGames,
  goTo,
};

export default connect(mapStateToProps, actionCreators)(Games);
