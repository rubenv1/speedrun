import { Map } from 'immutable';
import RunsActionTypes from '../actionTypes/Runs';
import * as RequestState from '../lib/RequestState';

const INITIAL_STATE = new Map({
  firstRunByGameId: new Map(),
  requestState: RequestState.NOT_REQUESTED,
});

export default function(state = INITIAL_STATE, action) {
  switch (action.type) {
    case RunsActionTypes.REQUEST_RUNS:
      return state.set('requestState', RequestState.REQUESTED);
    case RunsActionTypes.REQUEST_RUNS_SUCCESS:
      return state.set('requestState', RequestState.SUCCEEDED);
    case RunsActionTypes.REQUEST_RUNS_ERROR:
      return state.set('requestState', RequestState.FAILED);
    case RunsActionTypes.SET_FIRST_RUN:
      return state.set(
        'firstRunByGameId', 
        state.get('firstRunByGameId').merge({
          [action.payload.gameId]: action.payload
        })
      );
    default:
      return state;
  };
};
