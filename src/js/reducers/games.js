import { Map } from 'immutable';
import GamesActionTypes from '../actionTypes/Games';
import * as RequestState from '../lib/RequestState';

const INITIAL_STATE = new Map({
  games: new Map(),
  requestState: RequestState.NOT_REQUESTED,
});

export default function(state = INITIAL_STATE, action) {
  switch (action.type) {
    case GamesActionTypes.REQUEST_GAMES:
      return state.set('requestState', RequestState.REQUESTED);
    case GamesActionTypes.REQUEST_GAMES_SUCCESS:
      return state.set('requestState', RequestState.SUCCEEDED);
    case GamesActionTypes.REQUEST_GAMES_ERROR:
      return state.set('requestState', RequestState.FAILED);
    case GamesActionTypes.SET_GAMES:
      return state.set('games', action.payload);
    default:
      return state;
  };
};
