import { Map } from 'immutable';
import UsersActionTypes from '../actionTypes/Users';
import * as RequestState from '../lib/RequestState';

const INITIAL_STATE = new Map({
  users: new Map(),
  requestState: RequestState.NOT_REQUESTED,
});

export default function(state = INITIAL_STATE, action) {
  switch (action.type) {
    case UsersActionTypes.REQUEST_USER:
      return state.set('requestState', RequestState.REQUESTED);
    case UsersActionTypes.REQUEST_USER_SUCCESS:
      return state.set('requestState', RequestState.SUCCEEDED);
    case UsersActionTypes.REQUEST_USER_ERROR:
      return state.set('requestState', RequestState.FAILED);
    case UsersActionTypes.SET_USER:
      return state.set(
        'users', 
        state.get('users').merge({
          [action.payload.id]: action.payload,
        })
      );
    default:
      return state;
  };
};
