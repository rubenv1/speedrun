import { combineReducers } from 'redux';

import games from './games';
import runs from './runs';
import users from './users';

export default combineReducers({
  games,
  runs,
  users,
});
