const getRoot = state => state.users;

export const selectUser = (state, userId) => getRoot(state).get('users').get(userId);
export const selectUserRequestState = state => getRoot(state).get('requestState');
