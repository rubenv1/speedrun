const getRoot = state => state.games;

export const selectGames = state => getRoot(state).get('games') || [];
export const selectGame = (state, gameId) => getRoot(state).get('games').get(gameId);

export const selectGamesRequestState = state => getRoot(state).get('requestState');
