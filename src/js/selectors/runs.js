const getRoot = state => state.runs;

export const selectFirstRun = (state, gameId) => getRoot(state).get('firstRunByGameId').get(gameId);
export const selectRunsRequestState = state => getRoot(state).get('requestState');
