const axios = require('axios');

const GAMES_ENDPOINT = 'https://www.speedrun.com/api/v1/games';

export const getGames = () => {
  return axios.get(GAMES_ENDPOINT);
}
