const axios = require('axios');

const USER_ENDPOINT = "https://www.speedrun.com/api/v1/users";

export const getUser = (userId) => {
  return axios.get(`${USER_ENDPOINT}/${userId}`);
}
