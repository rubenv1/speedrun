const axios = require('axios');

const RUNS_ENDPOINT = 'https://www.speedrun.com/api/v1/runs';

export const getRuns = (gameId) => {
  return axios.get(RUNS_ENDPOINT, {
    params: {
      game: gameId,
    },
  });
}
