import keyMirror from 'fbjs/lib/keyMirror';

export default keyMirror({
  REQUEST_GAMES: null,
  REQUEST_GAMES_SUCCESS: null,
  REQUEST_GAMES_ERROR: null,
  SET_GAMES: null,
});
