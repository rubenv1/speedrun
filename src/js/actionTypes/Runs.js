import keyMirror from 'fbjs/lib/keyMirror';

export default keyMirror({
  REQUEST_RUNS: null,
  REQUEST_RUNS_SUCCESS: null,
  REQUEST_RUNS_ERROR: null,
  SET_FIRST_RUN: null,
});
