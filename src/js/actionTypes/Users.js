import keyMirror from 'fbjs/lib/keyMirror';

export default keyMirror({
  REQUEST_USER: null,
  REQUEST_USER_SUCCESS: null,
  REQUEST_USER_ERROR: null,
  SET_USER: null,
});
